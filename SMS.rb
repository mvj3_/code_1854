import android.os.Bundle;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.telephony.gsm.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SMSDemo extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		final EditText phonenumber = (EditText)findViewById(R.id.phonenumber);
		final EditText smsText = (EditText)findViewById(R.id.text);
		final Button sendSMS = (Button)findViewById(R.id.send);
		
		sendSMS.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String phoneNo = phonenumber.getText().toString();
				String message = smsText.getText().toString();
				
				if (phoneNo.length()>0 && message.length()>0) {
					sendSMS(phoneNo,message);
				} else {
					Toast.makeText(SMSDemo.this, "请重新输入电话号码和短信内容", Toast.LENGTH_LONG).show();
				}
			}
		});	
	}
    private void sendSMS(String phoneNo, String message) {
    	SmsManager sms = SmsManager.getDefault();
        PendingIntent pi = PendingIntent.getActivity(this, 0, 
                    new Intent(this, SMSDemo.class), 0);
        sms.sendTextMessage(phoneNo, null, message, pi, null);
		
	}
}
